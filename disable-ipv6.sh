#!/bin/bash
#
# disable-ipv6.sh

CWD=$(pwd)

echo "Disabling IPv6"
rm -f /etc/sysctl.d/enable-ipv6.conf
cp ${CWD}/sysctl.d/disable-ipv6.conf /etc/sysctl.d/
sysctl -p --load /etc/sysctl.d/disable-ipv6.conf > /dev/null

# Reconfigure SSH
if [ -f /etc/ssh/sshd_config ]
then
  echo "Configuring SSH server for IPv4 only"
  sed -i -e 's/#AddressFamily any/AddressFamily inet/g' /etc/ssh/sshd_config
  sed -i -e 's/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
  systemctl reload sshd
fi

# Reconfigure Postfix
if [ -f /etc/postfix/main.cf ]
then
  echo "Configuring Postfix server for IPv4 only"
  sed -i -e 's/# Enable IPv4, and IPv6 if supported/# Enable IPv4 only/g' /etc/postfix/main.cf
  sed -i -e 's/^inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
  systemctl restart postfix
fi


