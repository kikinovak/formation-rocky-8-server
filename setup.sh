#!/bin/bash
#
# setup.sh

CWD=$(pwd)

SLEEP=1

USERS=$(awk -F: '$3 > 999 && $3 < 65534 {print $1}' /etc/passwd | sort)

TOOLS=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkglists/tools.txt)

echo "Installing custom .bashrc for user: root"
cp -f ${CWD}/bash/bashrc-root /root/.bashrc
sleep ${SLEEP}

if [ ! -z "${USERS}" ]
then
  for USER in ${USERS}
  do
    if [ -d /home/${USER} ]
    then
      echo "Installing custom .bashrc for user: ${USER}"
      cp -f ${CWD}/bash/bashrc-users /home/${USER}/.bashrc
      chown ${USER}:${USER} /home/${USER}/.bashrc
      sleep ${SLEEP}
    fi
  done
fi

echo "Installing custom .bashrc for future users"
cp -f ${CWD}/bash/bashrc-users /etc/skel/.bashrc
sleep ${SLEEP}

echo "Installing custom .vimrc for user: root"
cp -f ${CWD}/vim/vimrc /root/.vimrc
sleep ${SLEEP}

if [ ! -z "${USERS}" ]
then
  for USER in ${USERS}
  do
    if [ -d /home/${USER} ]
    then
      echo "Installing custom .vimrc for user: ${USER}"
      cp -f ${CWD}/vim/vimrc /home/${USER}/.vimrc
      chown ${USER}:${USER} /home/${USER}/.vimrc
      sleep ${SLEEP}
    fi
  done
fi

echo "Installing custom .vimrc for future users"
cp -f ${CWD}/vim/vimrc /etc/skel/.vimrc
sleep ${SLEEP}

echo "Configuring SSH server"
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd
sleep ${SLEEP}

echo "Configuring persistent password for sudo"
cp -f ${CWD}/sudoers.d/persistent_password /etc/sudoers.d/
sleep ${SLEEP}

echo "Removing existing repositories"
rm -f /etc/yum.repos.d/*.repo
rm -f /etc/yum.repos.d/*.rpmsave
sleep ${SLEEP}

for REPOSITORY in BaseOS AppStream Extras PowerTools
do
  echo "Enabling repository: ${REPOSITORY}"
  cp -f ${CWD}/dnf/Rocky-${REPOSITORY}.repo /etc/yum.repos.d/
  sleep ${SLEEP}
done

echo "Enabling repository: EPEL"
if ! rpm -q epel-release > /dev/null 2>&1
then
  dnf install -y epel-release > /dev/null
fi
cp -f ${CWD}/dnf/epel.repo /etc/yum.repos.d/
sleep ${SLEEP}

echo "Enabling repository: EPEL Modular"
cp -f ${CWD}/dnf/epel-modular.repo /etc/yum.repos.d/
sleep ${SLEEP}

echo "Removing repository: EPEL Testing"
rm -f /etc/yum.repos.d/epel-testing.repo
sleep ${SLEEP}

echo "Removing repository: EPEL Testing Modular"
rm -f /etc/yum.repos.d/epel-testing-modular.repo
sleep ${SLEEP}

echo "Enabling repository: ELRepo"
if ! rpm -q elrepo-release > /dev/null 2>&1
then
  dnf install -y elrepo-release > /dev/null
fi
cp -f ${CWD}/dnf/elrepo.repo /etc/yum.repos.d/
sleep ${SLEEP}

echo "Fetching missing packages from Core package group"
dnf -y group mark remove "Core" > /dev/null 2>&1 
dnf -y group install "Core" > /dev/null
echo "Core package group installed on the system"
sleep ${SLEEP}

echo "Fetching missing packages from Base package group"
sleep ${SLEEP}
echo "This might take a moment..."
dnf -y group mark remove "Base" > /dev/null 2>&1
dnf -y group install "Base" > /dev/null
echo "Base package group installed on the system"
sleep ${SLEEP}

echo "Installing some additional packages"
sleep ${SLEEP}
for PACKAGE in ${TOOLS}
do
  if ! rpm -q ${PACKAGE} > /dev/null 2>&1
  then
    echo "Installing package: ${PACKAGE}"
    dnf install -y ${PACKAGE} > /dev/null
    sleep ${SLEEP}
  fi
done
echo "Basic system tools installed on the system"
sleep ${SLEEP}

exit 0
