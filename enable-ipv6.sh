#!/bin/bash
#
# enable-ipv6.sh

CWD=$(pwd)

echo "Enabling IPv6"
rm -f /etc/sysctl.d/disable-ipv6.conf
cp ${CWD}/sysctl.d/enable-ipv6.conf /etc/sysctl.d/
sysctl -p --load /etc/sysctl.d/enable-ipv6.conf > /dev/null

# Reconfigure SSH
if [ -f /etc/ssh/sshd_config ]
then
  echo "Configuring SSH server for IPv6"
  sed -i -e 's/^AddressFamily inet/#AddressFamily any/g' /etc/ssh/sshd_config
  sed -i -e 's/^ListenAddress 0.0.0.0/#ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
  systemctl reload sshd
fi

# Reconfigure Postfix
if [ -f /etc/postfix/main.cf ]
then
  echo "Configuring Postfix server for IPv6"
  sed -i -e 's/# Enable IPv4 only/# Enable IPv4, and IPv6 if supported/g' /etc/postfix/main.cf
  sed -i -e 's/^inet_protocols = ipv4/inet_protocols = all/g' /etc/postfix/main.cf
  systemctl restart postfix
fi
