#!/bin/bash
#
# reset.sh

CWD=$(pwd)

BASE=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkglists/base.txt)

echo "Restore enhanced base system"
echo "Creating database"
TMP="/tmp"
PKGLIST="${TMP}/pkglist"
PKGINFO="${TMP}/pkg_base"
rpm -qa --queryformat '%{NAME}\n' | sort > ${PKGLIST}
PACKAGES=$(egrep -v '(^\#)|(^\s+$)' $PKGLIST)
rm -rf ${PKGLIST} ${PKGINFO}
mkdir ${PKGINFO}
unset REMOVE
for PACKAGE in ${BASE}
do
  touch ${PKGINFO}/${PACKAGE}
done
for PACKAGE in ${PACKAGES}
do
  if [ -r ${PKGINFO}/${PACKAGE} ]
  then
    continue
  else
    REMOVE="${REMOVE}\n  * ${PACKAGE}"
  fi
done
if [ ! -z "${REMOVE}" ]
then
  echo "The following packages are not part of the enhanced base system:"
  echo -e "${REMOVE}"
fi
rm -rf ${PKGLIST} ${PKGINFO}
echo

